import Item from './components/Item';
import React, { Component } from "react";

export default class App extends Component {

  state = {
    theCart: ["YAMAHA Tracer 900", "TESLA Model X"],
    input: ""
  }; 
  

  addNewItem = () => {
    let { theCart, input } = this.state;
    theCart.push(input);
    input = "";
    this.setState({theCart: theCart, input: input})
  }

  saveInput = (e) => {
    this.setState({ input: e.target.value });
  };

  render() {
    return (
      <div className="App d-flex flex-column w-50 gap-3 mt-5">
        <h1>Liste des courses</h1>
          <ul className='list-group'>
            {this.state.theCart.map((item, sIndex) => {
              return <Item key={sIndex} name={item} />
            })}
          </ul>
        <label className='form-label mb-0' htmlFor='text-new-item' >Ajouter un nouvel item à la liste</label>
        <div className='d-flex'>
          <input type="text" className='form-control' name="text-new-item" id="text-new-item" value={this.state.input} onChange={this.saveInput} />
          <button className='btn btn-outline-primary ms-3 w-25' onClick={this.addNewItem}>Ajouter</button>
        </div>
        
      </div>
    );
  }
}
