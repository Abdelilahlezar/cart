import React from 'react'

const Item = ({ name }) => {
const [checked, setChecked] = React.useState(false);
  return (
    <li className='list-group-item'>
        <div className='form-check'>
          <input className='form-check-input' type="checkbox" defaultChecked={checked} onChange={() => setChecked(!checked)}></input>
          <label className='form-check-label' style={checked ? {textDecoration: "line-through"} : {}} >{name}</label>
        </div>
    </li>
  )
}


export default Item